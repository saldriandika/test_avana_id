<?php

	function findIndex($str, $i){

		$split 				= str_split($str);
		$cut				= substr($str, $i);	
		$pair 				= str_split($cut);

		$open = 0;
		foreach ($pair as $k => $v) {
			if($v == "("){
				$open++;
			}
			if($v == ")"){
				break;
			}		
		}		

		$close 	= 0;
		$h="";
		foreach ($pair as $k => $v) {
			if($v != ")"){
				$h = $v;
			}
			if($v == ")"){
				$close++;
			}

			if($close == $open){
				break;
			}		
		}
		$result = null;	
		foreach ($split as $k => $v) {		
			$result = $k;
			if($v == $h){
				break;
			}		
		}
		$result += 1;
		echo $result;		
	}

	$str = "a (b c (d e (f) g) h) i (j k)";
	$i   = 24; //2,7,12,24 
	findIndex($str, 2);
?>