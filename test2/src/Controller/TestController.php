<?php
namespace App\Controller;

use App\Files\File;

class TestController
{
    public function index()
    {	
    	$file   = File::fileA();
    	// $file   = File::fileB();

		$inputFileType 	= \PhpOffice\PhpSpreadsheet\IOFactory::identify($file);
		$reader 		= \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
		$spreadsheet 	= $reader->load($file);
		$worksheet 		= $spreadsheet->getActiveSheet();

		$highestRow 	= $worksheet->getHighestRow();
		$highestColumn 	= $worksheet->getHighestColumn();		
		$highestColumn++;
		
		$err = [];
		for ($row = 1; $row <= $highestRow; ++$row) {
		    
		    for ($col = 'A'; $col != $highestColumn; ++$col) {
		        
		        if(empty($worksheet->getCell('A' . $row)->getValue())){
		        	$err['A']['row '.$row] = "Missing value in " . $worksheet->getCell('A' . 1)->getValue();
		        }

		        if(preg_match('/\s/',$worksheet->getCell('B' . $row)->getValue())){
		        	$err['B']['row '.$row] = $worksheet->getCell('B' . 1)->getValue() . " should not contain any space ";
		        }

		        if(empty($worksheet->getCell('D' . $row)->getValue())){
		        	$err['D']['row '.$row] = "Missing value in " . $worksheet->getCell('D' . 1)->getValue();
		        }

		        if(empty($worksheet->getCell('E' . $row)->getValue())){
		        	$err['E']['row '.$row] = "Missing value in " . $worksheet->getCell('E' . 1)->getValue();
		        }

		    }		    
		}

		echo '<pre>', print_r($err,1),'</pre>';
    }
}

