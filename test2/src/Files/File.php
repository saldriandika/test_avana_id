<?php
namespace App\Files;

class File
{
    public function fileA()
    {	
    	$file = __DIR__ . '/Type_A.xls';
    	return $file;
    }

    public function fileB()
    {	
    	$file = __DIR__ . '/Type_B.xlsx';
    	return $file;
    }
}
